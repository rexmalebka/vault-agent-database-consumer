export MONGO_UNAME="mongo"
export MONGO_PASS="mongo"
export MONGO_ADDR="172.17.0.2"

# enable database
vault secrets disable database
vault secrets enable database

# write role on vault
vault write database/config/mongo \
    plugin_name=mongodb-database-plugin \
    allowed_roles="consumer" \
    connection_url="mongodb://{{username}}:{{password}}@$MONGO_ADDR:27017/admin?tls=false" \
    username=$MONGO_UNAME \
    password=$MONGO_PASS 


# Configure mongodb creation, revocation statements and ttls
vault write database/roles/consumer \
    db_name=mongo \
    creation_statements='{ "db": "admin", "roles": [{ "role": "readWrite" }, {"role": "read", "db": "foo"}] }' \
    revocation_statements='{ "db": "admin"}
'
    default_ttl="100s" \
    max_ttl="240s"


# create policy for app role
vault policy write agent policy.hcl

# enable approle and configure it
vault auth disable approle
vault auth enable approle

# configure token ttl for approle
vault write auth/approle/role/agent \
    token_ttl=60s \
    token_max_ttl=120s \
    token_policies="agent"

# export role id and secret id for vault agent
vault read -field=role_id auth/approle/role/agent/role-id > /tmp/role_id.txt
vault write -f -field=secret_id auth/approle/role/agent/secret-id > /tmp/secret_id.txt

vault agent -config=config.hcl -log-level=debug

