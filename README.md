# Vault Agent Proof of Concept with database creds

## Vault Agent

Vault Agent is a client daemon that provides automatic authentication to Vault, caching of responses containing newly created tokens and rendering user supplied templates.

## steps

1.- Run mongodb container
```
./run_mongodb.sh # modify username and password
```

2.- run script for agent, modify mondo username, password, address and TTL for creds

```
VAULT_TOKEN=root VAULT_ADDR='http://127.0.0.1:8200' ./run_agent.sh

```

3.- `template.ctmpl` renders creds file

4.- Run consumer app

```
./app.sh
```


